---
Title: Contact me
---
**Email**: `[my first name from the site footer]` at `[this site's domain name]`

**GitHub**: [@swinslow](https://github.com/swinslow)

**LinkedIn**: https://www.linkedin.com/in/stephen-winslow-5384167a/

I'm not on other social network things.
