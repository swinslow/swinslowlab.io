---
Title: License diffs
---

This site is intended to highlight differences between licenses that have very similar texts, and are sometimes hard to distinguish.

All licenses are referenced using identifiers from the [SPDX License List](https://spdx.org/licenses).

## Licenses

### BSD-3-Clause

* [BSD-3-Clause-Clear](BSD-3-Clause-Clear)
* [Sleepycat](Sleepycat)

## Caveats

Although I'm involved with the [SPDX project](https://spdx.org), the comments on this site are mine alone and do not necessarily reflect the viewpoints of the SPDX community.

Although I'm a lawyer, I'm not your lawyer, and the comments on this site should not be taken as legal advice.
