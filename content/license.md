---
Title: Site license details
---
All site content originally created by me is licensed under Creative Commons Attribution 4.0 International, [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html).

Additionally, at your option, all source code on this site originally created by me is also licensed under the [MIT](https://spdx.org/licenses/MIT.html) license.

If you would like to use any of the above under a different [OSI-approved](https://opensource.org/licenses/category) or [FSF-designated-as-free](https://www.gnu.org/licenses/license-list.en.html) or [Creative Commons](https://creativecommons.org/licenses/) license, [contact me](/contact) with a compelling reason why, and I'll probably say yes.

Any other third party content on the site is used under the license(s) indicated therein, or under [fair use](https://www.copyright.gov/title17/92chap1.html#107) or other similar concepts under other countries' copyright laws, or I dunno, maybe something else.
